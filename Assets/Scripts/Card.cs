﻿using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour{
    public int value;
    public int type;
    public int index;
    public GameManager manager;

    private void OnMouseUp() {
        if(type!=0)
            manager.Clicked(value, type, index, gameObject);
    }
}
