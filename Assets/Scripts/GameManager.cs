﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public GameObject startButton;
    public Text userText;
    public Text aiText;
    public Transform aiCardPlace;
    public Transform userCardPlace;
    public Transform allCardsPlace;
    public Transform openedCardsPlace;
    public GameObject[] cards;

    private Card[] aiCards;
    private Card[] userCards;
    private Card[] openedCards;
    private Card[] closedCards;
    private Card[] leftCards;
    private GameObject[] aiCardViews;
    private int lastOpenedIndex = 0;
    private GameObject opened;
    private bool isAIPlaying = false;
    private int userCardCount = 0;
    private int aiCardCount = 0;
    private int userScore = 0;
    private int aiScroe = 0;

    // Use this for initialization
    void Start () {
        for(int i = 0; i < cards.Length; i++) {
            cards[i].GetComponent<Card>().manager = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame() {
        startButton.SetActive(false);
        ResetGame();
    }

    private void ResetGame() {
        aiCards = new Card[4];
        userCards = new Card[4];
        openedCards = new Card[52];
        closedCards = new Card[3];
        leftCards = new Card[52];
        for(int i = 0; i < 52; i++) {
            Card c = cards[i].GetComponent<Card>();
            leftCards[i] = c;
        }

        int index;
        System.Random rand = new System.Random();
        for (int i = 0; i < 3; i++) {
            index = rand.Next(0, 51);
            while (leftCards[index]==null) {
                index = rand.Next(0, 51);
            }
            closedCards[i] = leftCards[index];
            leftCards[index] = null;
        }

        for (int i = 0; i < 4; i++) {
            index = rand.Next(0, 51);
            while (leftCards[index]==null) {
                index = rand.Next(0, 51);
            }
            aiCards[i] = leftCards[index];
            aiCards[i].index = i;
            leftCards[index] = null;
            while (leftCards[index]==null) {
                    index = rand.Next(0, 51);
                }
                userCards[i] = leftCards[index];
            userCards[i].index = i;
            leftCards[index] = null;
        }

         index = rand.Next(0, 51);
         while (leftCards[index]==null) {
             index = rand.Next(0, 51);
         }
         openedCards[0] = leftCards[index];
         leftCards[index] = null;

         GameObject back = (GameObject)Instantiate(cards[52], allCardsPlace.position, allCardsPlace.rotation);

        ShowCards();
    }

    private int cardToIndex(Card c) {
        return (c.type-1) * 13 + c.value-1;
    }

    private void ShowCards() {
        int index = 0;
        int indexai = 0;
        aiCardViews = new GameObject[4];
        for(int i = 0; i < userCards.Length; i++) {
            if (userCards[i] != null) {
                Vector3 pos = userCardPlace.position + Vector3.right * index * 1.5f;
                GameObject card = (GameObject)Instantiate(cards[cardToIndex( userCards[i])], pos, userCardPlace.rotation);
                index++;

            }

            if (aiCards[i] != null) {
                Vector3 pos = aiCardPlace.position + Vector3.right * indexai * 1.5f;
                GameObject card = (GameObject)Instantiate(cards[52], pos, aiCardPlace.rotation);
                aiCardViews[i] = card;
                indexai++;
            }
        }

        if (opened != null)
            Destroy(opened);
        opened = (GameObject)Instantiate(cards[cardToIndex(openedCards[lastOpenedIndex])], openedCardsPlace.position, openedCardsPlace.rotation);
    }

    public void Clicked(int value, int type, int index, GameObject obj) {
        if (isAIPlaying)
            return;
        
        int oldOpenedValue = openedCards[lastOpenedIndex==-1 ? 0 : lastOpenedIndex]==null ? -2:openedCards[lastOpenedIndex].value;
        lastOpenedIndex++;
        openedCards[lastOpenedIndex] = userCards[index];
        userCards[index] = null;
        obj.transform.position = openedCardsPlace.position;
        Destroy(opened);
        opened = obj;

        if (value == 11) {;
            CollectGround(true);
        }else if (oldOpenedValue == value) {
            CollectGround(true);
        } else {
            PlayAI();
        }
    }

    private void CollectGround(bool forUser) {
        int delta = 0;
        for (int i = 0; i < openedCards.Length; i++) {
            if (openedCards[i] != null) {
                delta += CardValue(openedCards[i].type, openedCards[i].value);
            }
        }

        if (openedCards[2] == null) {
            delta += openedCards[lastOpenedIndex].value == 11 ? 20 : 10;
        }

        if (forUser)
            ChangeUserScore(delta);
        else
            ChangeAIScore(delta);
        StartCoroutine(CleanUp(forUser));
    }

    public void ChangeUserScore(int delta) {
        userScore += delta;
        userText.text = "Oyuncu (" + userScore + ")";
    }

    public void ChangeAIScore(int delta) {
        aiScroe += delta;
        aiText.text = "Bilgisayar (" + aiScroe + ")";
    }

    public void PlayAI() {
        isAIPlaying = true;
        StartCoroutine("AIPlaying");
    }

    private int CardValue(int type, int value) {
        if (type == 3 && value == 2)
            return 2;
        else if (type == 1 && value == 10)
            return 3;
        else if (value == 1 && value == 11)
            return 1;
        else
            return 0;
    }

    IEnumerator CleanUp(bool nextAI) {
        yield return new WaitForSeconds(0.5f);
        Destroy(opened);
        for(int i = 0; i < openedCards.Length; i++) {
            openedCards[i] = null;
        }
        lastOpenedIndex = -1;
        if (nextAI) {
            PlayAI();
        }
    }

    IEnumerator AIPlaying() {
        yield return new WaitForSeconds(0.5f);
        bool isPlayed = false;

        if (lastOpenedIndex == -1) {
            randomAIPlay();
        } else {
            for (int i = 0; i < aiCards.Length; i++) {
                if ((aiCards[i] != null) && (openedCards[lastOpenedIndex] != null))
                    if (openedCards[lastOpenedIndex].value == aiCards[i].value || aiCards[i].value==11) {
                        lastOpenedIndex++;
                        openedCards[lastOpenedIndex] = aiCards[i];
                        print(aiCards[i].value + " " + aiCards[i].type);
                        aiCards[i] = null;
                        Destroy(opened);
                        opened = (GameObject)Instantiate(cards[cardToIndex(openedCards[lastOpenedIndex])], openedCardsPlace.position, openedCardsPlace.rotation);
                        isPlayed = true;
                        
                        CollectGround(false);
                        break;
                    }
            }
            if (!isPlayed) {
                randomAIPlay();
            }
        }
        DeleteOneAICard();
        isAIPlaying = false;
    }

    private void randomAIPlay() {
        System.Random rand = new System.Random();
        int index = rand.Next(0, 4);
        while (aiCards[index] == null) {
            index = rand.Next(0, 4);
        }
        lastOpenedIndex++;
        openedCards[lastOpenedIndex] = aiCards[index];
        print(aiCards[index].value + " " + aiCards[index].type);
        aiCards[index] = null;
        Destroy(opened);
        opened = (GameObject)Instantiate(cards[cardToIndex(openedCards[lastOpenedIndex])], openedCardsPlace.position, openedCardsPlace.rotation);
    }

    private void DeleteOneAICard() {
        for (int i = 3; i > -1; i--) {
            if (aiCardViews[i] != null) {
                Destroy(aiCardViews[i]);
                aiCardViews[i] = null;
                break;
            }
        }

        CheckForNewCards();
    }

    private void CheckForNewCards() {
        bool isCardinHand = false;
        for(int i = 0; i < aiCards.Length; i++) {
            if (aiCards[i] != null) {
                isCardinHand = true;
                print("there is card");
            }
        }
        if (!isCardinHand) {
            int index;
            System.Random rand = new System.Random();
            for (int i = 0; i < 4; i++) {
                index = rand.Next(0, 51);
                while (leftCards[index] == null) {
                    index = rand.Next(0, 51);
                }
                aiCards[i] = leftCards[index];
                aiCards[i].index = i;
                leftCards[index] = null;
                while (leftCards[index] == null) {
                    index = rand.Next(0, 51);
                }
                userCards[i] = leftCards[index];
                userCards[i].index = i;
                leftCards[index] = null;
            }
            print("showw");
            ShowCards();
        }
    }
    
}
